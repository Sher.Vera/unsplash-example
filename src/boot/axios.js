import axios from 'axios'

const axiosInstance = axios.create({})

export default async ({ store, Vue }) => {
  // Vue.prototype.$axios = axios
  Vue.prototype.$api = axiosInstance

  axiosInstance.interceptors.response.use(function (response) {
    // console.log('axiosResponse', response)
    // Todo bien con la respuesta
    if (response.config.method === 'post') {
      if (response.status === 201) {
        localStorage.setItem('GM_SESSION_INFO', JSON.stringify(response.data))
      }
    }
    return response.data
  })
}
export { axiosInstance }
