
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'unsplash', component: () => import('pages/Index.vue') },
      { path: 'list', component: () => import('pages/ListImg.vue') },
      { path: 'example', component: () => import('pages/Example.vue') },
      { path: 'dashboard', component: () => import('pages/Apexchart.vue') },
      { path: 'reports', component: () => import('pages/Reports.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
