export const medio = [
  {
    id: 1,
    name: 'Television'
  },
  {
    id: 2,
    name: 'Cine'
  },
  {
    id: 3,
    name: 'Radio'
  },
  {
    id: 4,
    name: 'Impresos'
  },
  {
    id: 5,
    name: 'OOH'
  },
  {
    id: 6,
    name: 'line'
  }
]

export const categoria = [
  {
    id: 1,
    medio_id: 1,
    name: 'TV Paga'
  },
  {
    id: 2,
    medio_id: 1,
    name: 'TV Abierta'
  },
  {
    id: 3,
    medio_id: 2,
    name: 'Cine'
  },
  {
    id: 4,
    medio_id: 3,
    name: 'Radio'
  },
  {
    id: 5,
    medio_id: 4,
    name: 'Prensa'
  },
  {
    id: 6,
    medio_id: 4,
    name: 'Revista'
  },
  {
    id: 7,
    medio_id: 5,
    name: 'Outdoor'
  },
  {
    id: 8,
    medio_id: 5,
    name: 'Indoor'
  },
  {
    id: 9,
    medio_id: 6,
    name: 'Search'
  },
  {
    id: 10,
    medio_id: 6,
    name: 'Display'
  },
  {
    id: 11,
    medio_id: 6,
    name: 'Social Net'
  }
]

export const vehiculo = [
  {
    id: 1,
    categoria_id: 1,
    name: 'TV PAGA LOC CABLE'
  },
  {
    id: 2,
    categoria_id: 1,
    name: 'TV PAGA NAL SAT'
  },
  {
    id: 3,
    categoria_id: 1,
    name: 'TV PAGA NAL SAT'
  },
  {
    id: 4,
    categoria_id: 1,
    name: 'TV PAGA NAL NET'
  },
  {
    id: 5,
    categoria_id: 1,
    name: 'TV PAGA NAL SEG'
  },
  {
    id: 6,
    categoria_id: 1,
    name: 'TV PAGA NAL STREAM'
  },
  {
    id: 7,
    categoria_id: 2,
    name: 'TV ABIERTA NAL'
  },
  {
    id: 8,
    categoria_id: 2,
    name: 'TV ABIERTA LOC'
  },
  {
    id: 9,
    categoria_id: 2,
    name: 'TV ABIERTA BLOQ'
  },
  {
    id: 10,
    categoria_id: 3,
    name: 'EN SALA'
  },
  {
    id: 11,
    categoria_id: 3,
    name: 'EN COMPLEJO'
  },
  {
    id: 12,
    categoria_id: 4,
    name: 'RADIO NAL'
  },
  {
    id: 13,
    categoria_id: 4,
    name: 'RADIO LOC'
  },
  {
    id: 14,
    categoria_id: 4,
    name: 'RADIO BLOQ'
  },
  {
    id: 15,
    categoria_id: 5,
    name: 'PRENSA LOC'
  },
  {
    id: 16,
    categoria_id: 5,
    name: 'PRENSA ZONA'
  },
  {
    id: 17,
    categoria_id: 6,
    name: 'REVISTA NAL'
  },
  {
    id: 18,
    categoria_id: 6,
    name: 'REVISTA LOC'
  },
  {
    id: 19,
    categoria_id: 6,
    name: 'REVISTA DT LIMIT'
  },
  {
    id: 20,
    categoria_id: 7,
    name: 'CARTELERA'
  },
  {
    id: 21,
    categoria_id: 7,
    name: 'BILLBOARD ELECTRO'
  },
  {
    id: 22,
    categoria_id: 7,
    name: 'MURO GD'
  },
  {
    id: 23,
    categoria_id: 7,
    name: 'PUENTE'
  },
  {
    id: 24,
    categoria_id: 7,
    name: 'TUNEL'
  },
  {
    id: 25,
    categoria_id: 7,
    name: 'VALLA'
  },
  {
    id: 26,
    categoria_id: 7,
    name: 'VALLA ELECTRO'
  },
  {
    id: 27,
    categoria_id: 7,
    name: 'VALLA MOVIL'
  },
  {
    id: 28,
    categoria_id: 7,
    name: 'BARDA'
  },
  {
    id: 29,
    categoria_id: 7,
    name: 'AUTOBUS TRANS PUB'
  },
  {
    id: 30,
    categoria_id: 7,
    name: 'AUTOBUS ESC/OFNA'
  },
  {
    id: 31,
    categoria_id: 7,
    name: 'AUTOBUS FORANEO'
  },
  {
    id: 32,
    categoria_id: 7,
    name: 'TRAILER'
  },
  {
    id: 33,
    categoria_id: 7,
    name: 'TAXI'
  },
  {
    id: 34,
    categoria_id: 7,
    name: 'METRO'
  },
  {
    id: 35,
    categoria_id: 7,
    name: 'METROBUS'
  },
  {
    id: 36,
    categoria_id: 7,
    name: 'TREN'
  },
  {
    id: 37,
    categoria_id: 7,
    name: 'CARABANA'
  },
  {
    id: 38,
    categoria_id: 7,
    name: 'MUPPIE'
  },
  {
    id: 39,
    categoria_id: 7,
    name: 'CILINDRO'
  },
  {
    id: 40,
    categoria_id: 7,
    name: 'PARABUS'
  },
  {
    id: 41,
    categoria_id: 7,
    name: 'CABINA TEL'
  },
  {
    id: 42,
    categoria_id: 7,
    name: 'PUESTO'
  },
  {
    id: 43,
    categoria_id: 7,
    name: 'VALET'
  },
  {
    id: 44,
    categoria_id: 7,
    name: 'DIRIGIBLE'
  },
  {
    id: 45,
    categoria_id: 8,
    name: 'AEROPUERTO'
  },
  {
    id: 46,
    categoria_id: 8,
    name: 'C COMERCIAL'
  },
  {
    id: 47,
    categoria_id: 8,
    name: 'TIENDA DEPA'
  },
  {
    id: 48,
    categoria_id: 8,
    name: 'TIENDA AUTO'
  },
  {
    id: 49,
    categoria_id: 8,
    name: 'ESTADIO'
  },
  {
    id: 50,
    categoria_id: 8,
    name: 'ESPACIO PUBLICO'
  },
  {
    id: 51,
    categoria_id: 8,
    name: 'OFICINA'
  },
  {
    id: 52,
    categoria_id: 8,
    name: 'HOSPITAL'
  },
  {
    id: 53,
    categoria_id: 8,
    name: 'ESCUELA'
  },
  {
    id: 54,
    categoria_id: 8,
    name: 'LOC MINORISTA'
  },
  {
    id: 55,
    categoria_id: 8,
    name: 'FARMACIA'
  },
  {
    id: 56,
    categoria_id: 8,
    name: 'TIENDA'
  },
  {
    id: 57,
    categoria_id: 8,
    name: 'RESTAURANTE'
  },
  {
    id: 58,
    categoria_id: 9,
    name: 'ADWORDS'
  },
  {
    id: 59,
    categoria_id: 9,
    name: 'VIDEO'
  },
  {
    id: 60,
    categoria_id: 10,
    name: 'DSP NETWORK'
  },
  {
    id: 61,
    categoria_id: 10,
    name: 'SITIOS UNITARIOS'
  },
  {
    id: 62,
    categoria_id: 10,
    name: 'REDES SOCIALES'
  },
  {
    id: 63,
    categoria_id: 11,
    name: 'CONTENIDOS'
  }
]

export const canalEstacion = [
  {
    id: 1,
    vehiculo_id: 1,
    name: 'IZZI'
  },
  {
    id: 2,
    vehiculo_id: 1,
    name: 'MEGACABLE,'
  },
  {
    id: 3,
    vehiculo_id: 1,
    name: 'CABLEVISION'
  },
  {
    id: 4,
    vehiculo_id: 1,
    name: 'TELECABLE,'
  },
  {
    id: 5,
    vehiculo_id: 2,
    name: 'SKY'
  },
  {
    id: 6,
    vehiculo_id: 2,
    name: 'DISH'
  },
  {
    id: 7,
    vehiculo_id: 3,
    name: 'SONY'
  },
  {
    id: 8,
    vehiculo_id: 3,
    name: 'WARNER'
  },
  {
    id: 9,
    vehiculo_id: 3,
    name: 'DISCOVERY'
  },
  {
    id: 10,
    vehiculo_id: 3,
    name: 'ESPN'
  },
  {
    id: 11,
    vehiculo_id: 3,
    name: 'UNICABLE'
  },
  {
    id: 12,
    vehiculo_id: 3,
    name: 'CANAL2 DELAY'
  },
  {
    id: 13,
    vehiculo_id: 4,
    name: 'CANAL 2 TVSA SEG'
  },
  {
    id: 14,
    vehiculo_id: 4,
    name: 'CANAL 5 SEG'
  },
  {
    id: 15,
    vehiculo_id: 5,
    name: 'TOTAL PLAY'
  },
  {
    id: 16,
    vehiculo_id: 6,
    name: 'CANAL 2'
  },
  {
    id: 17,
    vehiculo_id: 6,
    name: 'CANAL 13'
  },
  {
    id: 18,
    vehiculo_id: 7,
    name: 'CANAL 4 CDMX'
  },
  {
    id: 19,
    vehiculo_id: 7,
    name: 'CANAL 4 GDL'
  },
  {
    id: 20,
    vehiculo_id: 7,
    name: 'CANAL 12 MTY'
  },
  {
    id: 21,
    vehiculo_id: 8,
    name: 'CANAL 2 BLOQUEO GDL'
  },
  {
    id: 22,
    vehiculo_id: 8,
    name: 'LEON'
  },
  {
    id: 23,
    vehiculo_id: 8,
    name: 'PUEBLA'
  },
  {
    id: 24,
    vehiculo_id: 9,
    name: 'SPTEO VIDEO'
  },
  {
    id: 25,
    vehiculo_id: 9,
    name: 'SPOTEO AUDIO'
  },
  {
    id: 26,
    vehiculo_id: 10,
    name: 'POSTER'
  },
  {
    id: 27,
    vehiculo_id: 10,
    name: 'PANTALLA'
  },
  {
    id: 28,
    vehiculo_id: 10,
    name: 'ACTIVACION'
  },
  {
    id: 29,
    vehiculo_id: 11,
    name: 'IMAGEN'
  },
  {
    id: 30,
    vehiculo_id: 11,
    name: '40 PRINCIPALES'
  },
  {
    id: 31,
    vehiculo_id: 11,
    name: 'K BUENA'
  },
  {
    id: 32,
    vehiculo_id: 12,
    name: 'LA PODEROSA'
  },
  {
    id: 33,
    vehiculo_id: 12,
    name: 'LA PRINCIPAL'
  },
  {
    id: 34,
    vehiculo_id: 13,
    name: 'FORMULA BLOQUEO GDL'
  },
  {
    id: 35,
    vehiculo_id: 13,
    name: 'MTY'
  },
  {
    id: 36,
    vehiculo_id: 14,
    name: 'REFORMA (CDMX)'
  },
  {
    id: 37,
    vehiculo_id: 14,
    name: 'EL INFORMADOR (GDL)'
  },
  {
    id: 38,
    vehiculo_id: 14,
    name: 'EL NORTE (MTY'
  },
  {
    id: 39,
    vehiculo_id: 15,
    name: 'EL DIARIO DE CD. SATELITE'
  },
  {
    id: 40,
    vehiculo_id: 16,
    name: 'EXPANSION'
  },
  {
    id: 41,
    vehiculo_id: 16,
    name: 'TV NOTAS'
  },
  {
    id: 42,
    vehiculo_id: 16,
    name: 'VANIDADES'
  },
  {
    id: 43,
    vehiculo_id: 17,
    name: 'VOGUE'
  },
  {
    id: 44,
    vehiculo_id: 18,
    name: 'GAMERS (SOLO SINALOA)'
  },
  {
    id: 45,
    vehiculo_id: 18,
    name: 'SANTA FE (SOLO ZONA SANTA FE)'
  },
  {
    id: 46,
    vehiculo_id: 18,
    name: 'GUIA POLANCO (SOLO POLANCO)'
  },
  {
    id: 47,
    vehiculo_id: 19,
    name: 'VALORES (SOLO OFICINAS GOB)'
  },
  {
    id: 48,
    vehiculo_id: 19,
    name: 'ANAHUAC (SOLO EN UNIVERSIDAD)'
  },
  {
    id: 49,
    vehiculo_id: 20,
    name: 'CARTELERA/BILLBOARD/ESPECTACULAR EN VIA PUBLICA SENCILLA'
  },
  {
    id: 50,
    vehiculo_id: 20,
    name: 'DOBLE'
  },
  {
    id: 51,
    vehiculo_id: 21,
    name: 'PANTALLAS EN VIA PUBLICA'
  },
  {
    id: 52,
    vehiculo_id: 22,
    name: 'MURO DE GRANDES DIMENSIONES EN EDIFICIOS'
  },
  {
    id: 53,
    vehiculo_id: 23,
    name: 'PUENTES PEATONALES EN VÍA PUBLICA'
  },
  {
    id: 54,
    vehiculo_id: 24,
    name: 'TUNEL VIAL'
  },
  {
    id: 55,
    vehiculo_id: 25,
    name: 'VALLAS A NIVEL DE PISO EN VIA PUBLICA'
  },
  {
    id: 56,
    vehiculo_id: 26,
    name: 'VALLA ELECTRONICA A NIVEL DE PISO EN VIA PUBLICA'
  },
  {
    id: 57,
    vehiculo_id: 27,
    name: 'VALLA EN PLATAFORMA DE CAMION'
  },
  {
    id: 58,
    vehiculo_id: 28,
    name: 'PINTA/ROTULACIÒN EN BARDAS'
  },
  {
    id: 59,
    vehiculo_id: 29,
    name: 'AUTOBUS DE TRANSPORTE PUBLICO INTEGRAL (TODO EL AUTOBUS)'
  },
  {
    id: 60,
    vehiculo_id: 29,
    name: 'MEDALLON'
  },
  {
    id: 61,
    vehiculo_id: 29,
    name: 'LATERAL'
  },
  {
    id: 62,
    vehiculo_id: 30,
    name: 'AUTOBUS INTEGRAL EN RUTAS DE TRANSPORTE ESCOLAR O DE OFICINAS'
  },
  {
    id: 63,
    vehiculo_id: 31,
    name: 'AUTOBUS DE TRANSPORTE FORANEO INTEGRAL (TODO EL AUTOBUS)'
  },
  {
    id: 64,
    vehiculo_id: 31,
    name: 'MEDALLON,'
  },
  {
    id: 65,
    vehiculo_id: 31,
    name: 'LATERAL,'
  },
  {
    id: 66,
    vehiculo_id: 32,
    name: 'TRAILERS DE CARGA EN AUTOPISTAS'
  },
  {
    id: 67,
    vehiculo_id: 33,
    name: 'TAXIS PARTICULARES'
  },
  {
    id: 68,
    vehiculo_id: 33,
    name: 'SITIO'
  },
  {
    id: 69,
    vehiculo_id: 33,
    name: 'UBER'
  },
  {
    id: 70,
    vehiculo_id: 34,
    name: 'PANTALLAS'
  },
  {
    id: 71,
    vehiculo_id: 34,
    name: 'VALLAS'
  },
  {
    id: 72,
    vehiculo_id: 34,
    name: 'COLUMNAS'
  },
  {
    id: 73,
    vehiculo_id: 34,
    name: 'POSTER EN PASILLOS'
  },
  {
    id: 74,
    vehiculo_id: 34,
    name: 'VAGONES DE METRO'
  },
  {
    id: 75,
    vehiculo_id: 35,
    name: 'PANTALLAS'
  },
  {
    id: 76,
    vehiculo_id: 35,
    name: 'VALLAS'
  },
  {
    id: 77,
    vehiculo_id: 35,
    name: 'COLUMNAS'
  },
  {
    id: 78,
    vehiculo_id: 35,
    name: 'POSTER EN PASILLOS'
  },
  {
    id: 79,
    vehiculo_id: 35,
    name: 'VAGONES DE METROBUS'
  },
  {
    id: 80,
    vehiculo_id: 36,
    name: 'PANTALLAS'
  },
  {
    id: 81,
    vehiculo_id: 36,
    name: 'VALLAS'
  },
  {
    id: 82,
    vehiculo_id: 36,
    name: 'COLUMNAS'
  },
  {
    id: 83,
    vehiculo_id: 36,
    name: 'POSTER EN PASILLOS'
  },
  {
    id: 84,
    vehiculo_id: 36,
    name: 'VAGONES DE TREN FORANEO'
  },
  {
    id: 85,
    vehiculo_id: 37,
    name: 'GRUPO DE VEHICULOS (AUTOS, BICICLETAS, MOTOS)'
  },
  {
    id: 86,
    vehiculo_id: 38,
    name: 'POSTER EN ESTRUCTURA DE 2 VISTAS EN VIA PUBLICA'
  },
  {
    id: 87,
    vehiculo_id: 39,
    name: 'POSTER EN ESTRUCTURA EN FORMA DE CILINDRO EN VIA PUBLICA'
  },
  {
    id: 88,
    vehiculo_id: 40,
    name: 'POSTER O MUPPIE EN PARADA DE AUTOBUS'
  },
  {
    id: 89,
    vehiculo_id: 41,
    name: 'CABINAS TELEFONICAS EN VIA PUBLICA'
  },
  {
    id: 90,
    vehiculo_id: 42,
    name: 'PUESTOS DE REVISTAS EN VÌA PUBLICA'
  },
  {
    id: 91,
    vehiculo_id: 42,
    name: 'FLORES EN VÌA PUBLICA'
  },
  {
    id: 92,
    vehiculo_id: 43,
    name: 'MUEBLE DE VALET PARKING EN RESTAURANTES'
  },
  {
    id: 93,
    vehiculo_id: 43,
    name: 'BARES'
  },
  {
    id: 94,
    vehiculo_id: 43,
    name: 'ANTROS'
  },
  {
    id: 95,
    vehiculo_id: 44,
    name: 'DIRIGIBLE A ESCALA'
  },
  {
    id: 96,
    vehiculo_id: 45,
    name: 'PANTALLAS EN AEROPUERTO'
  },
  {
    id: 97,
    vehiculo_id: 45,
    name: 'VALLAS EN AEROPUERTO'
  },
  {
    id: 98,
    vehiculo_id: 45,
    name: 'MUPPIES EN AEROPUERTO'
  },
  {
    id: 99,
    vehiculo_id: 45,
    name: 'MESAS EN AEROPUERTO'
  },
  {
    id: 100,
    vehiculo_id: 46,
    name: 'PANTALLAS EN CC SANTA FE, ANTARA, ANDARES, VALLE ORIENTE'
  },
  {
    id: 101,
    vehiculo_id: 46,
    name: 'VALLAS EN CC SANTA FE, ANTARA, ANDARES, VALLE ORIENTE'
  },
  {
    id: 102,
    vehiculo_id: 46,
    name: 'MUPPIES EN CC SANTA FE, ANTARA, ANDARES, VALLE ORIENTE'
  },
  {
    id: 103,
    vehiculo_id: 46,
    name: 'MESAS EN CC SANTA FE, ANTARA, ANDARES, VALLE ORIENTE'
  },
  {
    id: 104,
    vehiculo_id: 47,
    name: 'PANTALLAS EN PALACIO DE HIERRO, LIVERPOOL, SUBURBIA, COPEL'
  },
  {
    id: 105,
    vehiculo_id: 47,
    name: 'VALLAS EN PALACIO DE HIERRO, LIVERPOOL, SUBURBIA, COPEL'
  },
  {
    id: 106,
    vehiculo_id: 47,
    name: 'MUPPIES EN PALACIO DE HIERRO, LIVERPOOL, SUBURBIA, COPEL'
  },
  {
    id: 107,
    vehiculo_id: 47,
    name: 'MESAS EN PALACIO DE HIERRO, LIVERPOOL, SUBURBIA, COPEL'
  },
  {
    id: 108,
    vehiculo_id: 48,
    name: 'PANTALLAS EN WALMART, COSTCO, CHEDRAUI'
  },
  {
    id: 109,
    vehiculo_id: 48,
    name: 'VALLAS EN WALMART, COSTCO, CHEDRAUI'
  },
  {
    id: 110,
    vehiculo_id: 48,
    name: 'MUPPIES EN WALMART, COSTCO, CHEDRAUI'
  },
  {
    id: 111,
    vehiculo_id: 48,
    name: 'MESAS EN WALMART, COSTCO, CHEDRAUI'
  },
  {
    id: 112,
    vehiculo_id: 49,
    name: 'PANTALLAS EN ESTADIO'
  },
  {
    id: 113,
    vehiculo_id: 49,
    name: 'VALLAS EN ESTADIO'
  },
  {
    id: 114,
    vehiculo_id: 49,
    name: 'MUPPIES EN ESTADIO'
  },
  {
    id: 115,
    vehiculo_id: 49,
    name: 'MESAS EN ESTADIO'
  },
  {
    id: 116,
    vehiculo_id: 50,
    name: 'EXPLANADAS EN EXPLANADAS, PLAZAS, PARQUES'
  },
  {
    id: 117,
    vehiculo_id: 50,
    name: 'BANNERS EN EXPLANADAS, PLAZAS, PARQUES'
  },
  {
    id: 118,
    vehiculo_id: 50,
    name: 'KIOSKOS EN EXPLANADAS, PLAZAS, PARQUES'
  },
  {
    id: 119,
    vehiculo_id: 51,
    name: 'ELEVADORES EN OFICINAS PRIVADAS'
  },
  {
    id: 120,
    vehiculo_id: 51,
    name: 'ESTACIONAMIENTOS EN OFICINAS PRIVADAS'
  },
  {
    id: 121,
    vehiculo_id: 52,
    name: 'ELEVADORES EN HOSPITALES'
  },
  {
    id: 122,
    vehiculo_id: 52,
    name: 'ESTACIONAMIENTOS EN HOSPITALES'
  },
  {
    id: 123,
    vehiculo_id: 53,
    name: 'ELEVADORES EN ESCUELAS'
  },
  {
    id: 124,
    vehiculo_id: 53,
    name: 'ESTACIONAMIENTOS EN ESCUELAS'
  },
  {
    id: 125,
    vehiculo_id: 54,
    name: 'PTO DE VENTA EN OXXO, 7ELEVEN, STARBUCKS'
  },
  {
    id: 126,
    vehiculo_id: 54,
    name: 'PANTALLAS EN OXXO, 7ELEVEN, STARBUCKS'
  },
  {
    id: 127,
    vehiculo_id: 55,
    name: 'PTO DE VENTA EN FARMACIA GUADALAJARA, DR SIMI'
  },
  {
    id: 128,
    vehiculo_id: 55,
    name: 'PANTALLAS EN FARMACIA GUADALAJARA, DR SIMI'
  },
  {
    id: 129,
    vehiculo_id: 56,
    name: 'PTO DE VENTA EN TIENDAS DE CONVENIENCIA'
  },
  {
    id: 130,
    vehiculo_id: 56,
    name: 'PANTALLAS EN TIENDAS DE CONVENIENCIA'
  },
  {
    id: 131,
    vehiculo_id: 57,
    name: 'PTO DE VENTA EN RESTAURANTES'
  },
  {
    id: 132,
    vehiculo_id: 57,
    name: 'PANTALLAS EN RESTAURANTES'
  },
  {
    id: 133,
    vehiculo_id: 58,
    name: 'GOOGLE'
  },
  {
    id: 134,
    vehiculo_id: 59,
    name: 'YOUTUBE'
  },
  {
    id: 135,
    vehiculo_id: 60,
    name: 'COMPRA PROGRAMATICA DE VIDEO E IMAGEN EN REDES DE SITIOS'
  },
  {
    id: 136,
    vehiculo_id: 61,
    name: 'COMPRA DE VIDEO E IMAGEN EN UN SITIO DETERMINADO'
  },
  {
    id: 137,
    vehiculo_id: 62,
    name: 'PUBICIDAD EN IMAGEN DENTRO DE REDES SOCIALES'
  },
  {
    id: 138,
    vehiculo_id: 63,
    name: 'COMMUNITY MANAGER'
  },
  {
    id: 139,
    vehiculo_id: 63,
    name: 'BLOGS'
  },
  {
    id: 140,
    vehiculo_id: 63,
    name: 'CONTENIDOS'
  },
  {
    id: 141,
    vehiculo_id: 63,
    name: 'POST'
  },
  {
    id: 142,
    vehiculo_id: 63,
    name: 'TWIT'
  }
]
export const ciudad = [
  {
    id: 1,
    medio_id: [1, 2, 3, 4, 5, 6, 7],
    name: 'Ciudad de Mexico'
  },
  {
    id: 2,
    medio_id: [1, 2, 3, 4, 5, 6, 7],
    name: 'Sinaloa'
  }
]
